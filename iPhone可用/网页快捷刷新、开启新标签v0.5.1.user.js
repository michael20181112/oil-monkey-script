// ==UserScript==
// @name         刷新
// @namespace    https://itakeo.com
// @version      0.5.1
// @description  手动刷新
// @author       itakeo
// @match        *
// @match        *://*/*
// @grant        none
// ==/UserScript==


/*
    小红点脚本，主要是为了解决大屏或单手持手机时，对刷新，新建标签和关闭标签功能的简化操作。
    单机：刷新；
    双击：新建标签页;
    长按：关闭当前标签（关闭的标签需由红点打开的情况下）

    ver 0.5.1
    1、修复页面有iFrame嵌套时，出现多个红点
    2、修改双击，长按逻辑，增加容错率
*/

;(function(){

    if(window.top.document.querySelector('.zc_click_reload')) return;
    window.top.document.body.insertAdjacentHTML("afterend", `<div class="zc_click_reload" style="position: fixed;z-index: 999999;background: red;opacity:0.6;left: 50%;bottom:25px;margin-left:-12px; outline: none; cursor: pointer; width: 24px;height:24px;border-radius:50%;-webkit-user-select:none;user-select:none;transition: box-shadow 0.3s ease;-webkit-transition: box-shadow 0.3s ease;"></div><style>.zc_click_reload:active{ box-shadow: 0px 0px 0px 8px red }</style>`); 

    let reload_dom = document.querySelector('.zc_click_reload');
        reload_dom.takeo_click_temp = 0;
        reload_dom.takeo_click_timer = null;
    let startX = 0,startY = 0;
    let x = 0,y = 0,timeCount="";

    function _get(e){
        return [e.changedTouches[0].pageX,e.changedTouches[0].pageY]
    };

    // 手指触摸
    reload_dom.addEventListener('touchstart', function(e) {
        startX = _get(e)[0];
        startY = _get(e)[1];
        x = this.offsetLeft;
        y = this.offsetTop;
        timeCount = Date.now();
        e.preventDefault(); 
    });

    // 手指移动
    reload_dom.addEventListener('touchmove', function(e) {
        let moveX = _get(e)[0] - startX;
        let moveY = _get(e)[1] - startY;
        this.style.left = x + 12 + moveX + 'px';
        this.style.top = y + moveY + 'px';
        e.preventDefault(); 
    });

    // 手指释放
    reload_dom.addEventListener('touchend', function(e) {
        if(Math.abs(_get(e)[0] - startX ) <= 3 && Math.abs(_get(e)[1] - startY ) <= 3){
            if( Date.now()- timeCount >=500 ){ //长按
                window.close();
                return;
            };
            this.takeo_click_temp++;
            clearTimeout(this.takeo_click_timer);
            this.takeo_click_timer = setTimeout(()=>{
                //单击、双击
                if(this.takeo_click_temp>=2) window.open('https://www.baidu.com','_blank');  
                else  window.location.reload(); 
                this.takeo_click_temp = 0;
            },250);
        };
        e.preventDefault(); 
    });
})();